[GtkTemplate (ui = "/org/example/App/window.ui")]
public class ListDemoVala.Window : Gtk.ApplicationWindow {
    public Window (Gtk.Application app) {
        Object (application: app);
    }

    [GtkCallback]
    private string format_label (string format, string adj, bool flag) {
        if (flag)
            return format.printf (adj);

        return format.printf (@"not $adj");
    }
}
